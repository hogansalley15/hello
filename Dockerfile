FROM python:alpine

# Use this section if all dependencies are available on network
COPY requirements.txt /
RUN pip3 install --upgrade pip
RUN pip3 install --no-cache-dir -r requirements.txt

WORKDIR /app

COPY app.py .

ENTRYPOINT ["python"]

CMD ["app.py"]
