#!/usr/bin/env python
import os

# Simple test
name = os.environ.get('name')
if (name != None):
    print("Hello, " + name + " , welcome to GitLab!")
else: 
    print("Hello, World")